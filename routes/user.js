var mongoose = require('mongoose');
var express = require('express');
var jsonwebtoken = require('jsonwebtoken');
var router = express.Router();
var User = require("../model/user");
import Config from '../config/config';
var bcrypt = require('bcrypt-nodejs');

const {
  port,
  secretKey,
  expiredAfter
} = Config;

/************REGISTER API************************* */
router.post('/api/signup', function (req, res) {

  if (!req.body.name || !req.body.password || !req.body.email) {
    res.json({
      success: false,
      msg: 'Please enter email,name and password.'
    });
  } else {
    console.log(req.body);
    var newUser = new User({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      permission: req.body.permission,
      created: new Date().getTime(),
      deleted: req.body.deleted
    });
    // save the user
    newUser.save(function (err) {
      if (err) {

        if (err.name === 'BulkWriteError' && err.code === 11000) {
          console.log("test");
          return res.json({
            success: false,
            msg: 'Email Id already exist!.'
          });
        } else {
          console.log('err', err);
          return res.json({
            success: false,
            msg: 'Some thing is wrong.'
          });
        }
        //  return ;

      }
      res.json({
        success: true,
        msg: req.body.name + ' created successfully.'
      });
    });
  }
});

/**************END REGISTER API*************/

/**************LOGIN API********************/

router.post('/api/login', (req, res) => {
  const response = {};
  const update_login_date = {};
  update_login_date.login_date = new Date().getTime();
  User.findOne({
    email: req.body.username,
    deleted: 0
  }, function (err, user) {
    if (err) {
      // console.log(err);
      response.error = 'Internal Server Error.';
      res.json(response);
      //  res.status(500).send({success: false, msg: 'Internal Server Error.'});
    }
    if (!user) {
      response.error = 'Authentication failed. User not found.';
      res.json(response);
      // res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
    } else {
      // check if password matches
      //console.log(user);
      user.comparePassword(req.body.password, function (err, isMatch) {
        // console.log(isMatch);
        if (isMatch && !err) {
          // if user is found and password is right create a token
          response.token = jsonwebtoken.sign({
              expiredAt: new Date().getTime() + expiredAfter,
              email: user.email,
              id: user._id,
            },
            secretKey
          );

          User.findByIdAndUpdate(user._id, update_login_date, function (err, post) {
            if (err) {
              response.error = 'Authentication failed. Wrong creditinals.';
              res.json(response);
            }
            res.json(response);
          });

          // var token = jwt.sign(user, config.secret);
          // return the information including token as JSON
          // res.json({success: true, token:token});
        } else {
          response.error = 'Authentication failed. Wrong creditinals.';
          res.json(response);
        }
      });
    }

  });

});
/**************END LOGIN API****************/


/************* USER LIST API****************/
router.get('/api/userlist', function (req, res) {

  console.log('Pranav', req.query);
  /*********Search Query build ************/
  var dbquery = {};
  if (req.query.name) {
    dbquery.name = new RegExp(req.query.name, "i");
  }
  if (req.query.email) {
    dbquery.email = new RegExp(req.query.email, "i");
  }

  if (req.query.deleted) {
    dbquery.deleted = req.query.deleted;
  }

  if (req.query.permission) {
    var permission = req.query.permission;
    var permissionArray = permission.split(',');
    dbquery.permission = {
      $in: permissionArray
    }; //req.query.stream;
  }



  /******* pagination query started here ***********/
  var pageNo = parseInt(req.query.pageNo) //req.query.pageNo
  var size = parseInt(req.query.per_page) //
  var query = {}
  if (pageNo < 0 || pageNo === 0) {
    response = {
      success: false,
      message: "invalid page number, should start with 1"
    };
    return res.json(response)
  }
  query.skip = size * (pageNo - 1)
  query.limit = size
  console.log('query', query);

  /******* pagination query end here****************/

  /************total count query start here ********/
  // Find some documents
  User.find(dbquery).count().exec(function (err, totalCount) {
    console.log('totalCount', totalCount);
    if (err) {
      res.json({
        success: false,
        result: "Error fetching data"
      });
    }
    User.find(dbquery, {
        name: 1,
        email: 1,
        _id: 1,
        deleted: 1,
        created: 1,
        permission: 1,
        login_date: 1
      })
      .sort({
        _id: -1
      }).skip(query.skip).limit(query.limit)
      .exec(function (err, userInformation) {
        if (err) {
          res.json({
            success: false,
            result: 'error'
          });
        } else {
          //    var totalPages = Math.ceil(totalCount / size)
          res.json({
            success: true,
            result: userInformation,
            totalRecCount: totalCount
          });
        }
      });


  });

});
/************** END OF USER LIST API*************/
/************** START USER UPDATE API************/
router.put('/api/updateUser/:id', function (req, res, next) {
  console.log(req.params.id);
  console.log(req.body);


  //  console.log('After generate password');
  //  console.log(req.body);
  /*********** password set ******************/
  if (req.body.password) {
    //console.log('req.body.password',req.body.password);
    bcrypt.genSalt(10, function (err, salt) {
      bcrypt.hash(req.body.password, salt, null, function (err, hash) {
        //console.log('hash',hash);
        if (err) {
          //return cb(err);
          res.json({
            success: false,
            msg: "Someting wrong."
          });
        }
        if (hash) {
          req.body.password = hash;
          //  console.log('hash',req.body.password);
          User.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
            if (err) {
              //return next(err);
              res.json({
                success: false,
                msg: "Someting wrong."
              });
            }
            res.json({
              success: true,
              msg: req.body.name + " updated successfully"
            });
            // res.json(post);
          });
        }
        // cb(null, hash);
      });
    });

  } else {
    // res.json('test');
    User.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
      if (err) {
        return next(err);
      }
      res.json({
        success: true,
        msg: req.body.name + " updated successfully"
      });
      // res.json(post);
    });
  } // else end
});
/***************** END USER UPDATE API *************/
/************** START USER FETCH BY ID API************/
router.route("/api/user/id/:id")
  .get(function (req, res) {
    var response = {};
    User.findById(req.params.id, function (err, data) {
      // This will run Mongo Query to fetch data based on ID.
      if (err) {
        response = {
          "success": false,
          "message": "Error fetching data"
        };
      } else {
        response = {
          "success": true,
          "result": data
        };
      }
      res.json(response);
    });
  })
/***************** END USER FETCH BY ID API *************/
/************************** validate user email ***************/
router.route('/api/user/validate').get(function (req, res) {
  /************unique check *********/
  var dbbuildQuery = {};

  if (req.query.email) {
    dbbuildQuery.email = req.query.email;
  }
  User.find(dbbuildQuery).count().exec(function (err, totalCount) {
    if (err) {
      res.status(400).send("unable to save to database" + err);
    } else {
      res.json({
        success: true,
        result: totalCount
      });
    }


  });
});

module.exports = router;