var mongoose = require('mongoose');
var express = require('express');
var jsonwebtoken = require('jsonwebtoken');
var PacketRouter = express.Router();
var Packet = require("../model/packet_data");
import Config from '../config/config';
var bcrypt = require('bcrypt-nodejs');

PacketRouter.route('/api/packet').get(function(req, res) {
    Packet.findOne({}).sort({created: -1}).exec(function(err, data) {
        if(err) {
            console.log(err);
            res.status(400).json({
                msg: "something went wrong",
                err: err
            })
        } else {
            if(data) {
                console.log("packet found");
                res.status(200).json({
                    sucess: true,
                    data: data
                })
            } else {
                res.status(400).json({
                    msg: "no data packet found"
                })
            }
        }
    })
})

PacketRouter.route('/api/packet/all').get(function(req, res) {
    Packet.find({}).sort({created: -1}).limit(50).exec(function(err, data) {
        if(err) {
            console.log(err);
            res.status(400).json({
                msg: "something went wrong",
                err: err
            })
        } else {
            if(data.length > 0) {
                console.log("packet found");
                res.status(200).json({
                    sucess: true,
                    data: data
                })
            } else {
                res.status(400).json({
                    msg: "no data packet found"
                })
            }
        }
    })
})

module.exports = PacketRouter;