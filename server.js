import express from 'express';
import bodyParser from 'body-parser';
import jsonwebtoken from 'jsonwebtoken';
import cors from 'cors';
import Config from './config/config';
import Database from './config/db';
import {
  authenticate,
  authError
} from './middleware';
var mongoose = require('mongoose');
var register = require('./routes/user');
var packets = require('./routes/packets');
const http = require('http');
var app = express();
import {
  parseTowerData
} from './scripts/parseData'
// const server = http.Server(app);
// var io = require('socket.io')(server);
// app.listen(9000);



// io.on('connection', socket => {
//   console.log("new client connected")
//   socket.on('my-event', data => {
//     console.log(data, "this is data on the port")
//       io.emit("event-from-8000", "From 8000: " + data);
//   });
// });
var net = require('net');

var server = net.createServer(function (socket) {
  socket.setEncoding('utf8');
  socket.write('Ready to receive data packets');
  console.log("New Client connected")
  socket.on('data', function (data) {
    parseTowerData(data)

  });
});
server.listen(8000, process.env.IP);
//var api = require('./routes/api');

// Mongoose connection with mongodb
mongoose.Promise = require('bluebird');
mongoose.connect(Database.database)
  .then(() => { // if all is ok we will be here
    console.log('Start');
  })
  .catch(err => { // if error we will be here
    console.error('App starting error:', err.stack);
    process.exit(1);
  });

const {
  port,
  secretKey,
  expiredAfter
} = Config;

app.get('/', (req, res) => {
  res.json({
    status: 'OK'
  });
});
// Use middlewares to set view engine and post json data to the server
app.use(express.static('public'));
app.use(cors());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use('/', register);
app.use('/', packets);

//Start the server
app.listen(port, function () {
  console.log('Server is running on Port: ', port);
});



module.exports = app;