var Packet = require("../model/packet_data");

export function parseTowerData(data) {
    return new Promise(function (resolve, reject) {
        var str = "~197,RTWD,R,1234567890,190210,173057,1,1,1,0,1,0,1,0,0,0,1,1,0,4.5,10.7,25.7,38.4,220.7,224.5,210.8,0,0,0,0,0,0,0,0,0,1,28.2531,76.5234,3000,6,24,1,Airtel,625327989532146,195327953272183,0,0,0,0*116";
        var data_array = data.split(",");
        var obj = {};
        obj.add = {};
        console.log(data_array.length, "lenght of data array");
        if (data_array[1] === "RTWD") {
            obj['len'] = data_array[0];
            obj['pkt_name'] = data_array[1];
            obj['pkt_type'] = data_array[2];
            obj['device_id'] = parseFloat(data_array[3]);
            obj['date'] = parseFloat(data_array[4]);
            obj['time'] = parseFloat(data_array[5]);
            obj['block_no'] = parseFloat(data_array[6])
            obj.created = Date.now();
            obj.add['DIN1'] = parseFloat(data_array[7]);
            obj.add['DIN2'] = parseFloat(data_array[8]);
            obj.add['DIN3'] = parseFloat(data_array[9]);
            obj.add['DIN4'] = parseFloat(data_array[10]);
            obj.add['DIN5'] = parseFloat(data_array[11]);
            obj.add['DIN6'] = parseFloat(data_array[12]);
            obj.add['DIN7'] = parseFloat(data_array[13]);
            obj.add['DIN8'] = parseFloat(data_array[14]);
            obj.add['DIN9'] = parseFloat(data_array[15]);
            obj.add['DIN10'] = parseFloat(data_array[16]);
            obj.add['DIN11'] = parseFloat(data_array[17]);
            obj.add['DIN12'] = parseFloat(data_array[18]);
            obj.add['AIN1'] = parseFloat(data_array[19]);
            obj.add['AIN2'] = parseFloat(data_array[20]);
            obj.add['AIN3'] = parseFloat(data_array[21]);
            obj.add['AIN4'] = parseFloat(data_array[22]);
            obj.add['AIN5'] = parseFloat(data_array[23]);
            obj.add['AIN6'] = parseFloat(data_array[24]);
            obj.add['AIN7'] = parseFloat(data_array[25]);
            obj.add['AIN8'] = parseFloat(data_array[26]);
            obj.add['AIN9'] = parseFloat(data_array[27]);
            obj.add['AIN10'] = parseFloat(data_array[28]);
            obj.add['AIN11'] = parseFloat(data_array[29]);
            obj.add['AIN12'] = parseFloat(data_array[30]);
            obj.add['DO1'] = parseFloat(data_array[31]);
            obj.add['DO2'] = parseFloat(data_array[32]);
            obj.add['DO3'] = parseFloat(data_array[33]);
            obj.add['DO4'] = parseFloat(data_array[34]);
            obj.add['GPS_SYNC'] = parseFloat(data_array[35]);
            obj.add['Latitude'] = parseFloat(data_array[36]);
            obj.add['Longitude'] = parseFloat(data_array[37]);
            obj.add['Altitude'] = parseFloat(data_array[38]);
            obj.add['No_of_Satellite'] = parseFloat(data_array[39]);
            obj.add['GSM_Signal'] = parseFloat(data_array[40]);
            obj.add['Network'] = parseFloat(data_array[41]);
            obj.add['Operator'] = parseFloat(data_array[42]);
            obj.add['IMEI No'] = parseFloat(data_array[43]);
            obj.add['IMSI'] = parseFloat(data_array[44]);
            obj.add['Misc1'] = parseFloat(data_array[45]);
            obj.add['Misc2'] = parseFloat(data_array[46]);
            obj.add['Misc3'] = parseFloat(data_array[47]);
            obj.add['Misc4'] = parseFloat(data_array[48]);
        }

        if (data_array[1] === "ALRMD") {
            obj['len'] = data_array[0];
            obj['pkt_name'] = data_array[1];
            obj['pkt_type'] = data_array[2];
            obj['device_id'] = parseFloat(data_array[3]);
            obj['date'] = parseFloat(data_array[4]);
            obj['time'] = parseFloat(data_array[5]);
            obj.created = Date.now();
            obj.add['SEQ-NO'] = parseFloat(data_array[6]);
            obj.add['CDIN1'] = parseFloat(data_array[7]);
            obj.add['CDIN2'] = parseFloat(data_array[8]);
            obj.add['CDIN3'] = parseFloat(data_array[9]);
            obj.add['CDIN4'] = parseFloat(data_array[10]);
            obj.add['CDIN5'] = parseFloat(data_array[11]);
            obj.add['CDIN6'] = parseFloat(data_array[12]);
            obj.add['CDIN7'] = parseFloat(data_array[13]);
            obj.add['CDIN8'] = parseFloat(data_array[14]);
            obj.add['CDIN9'] = parseFloat(data_array[15]);
            obj.add['CDIN10'] = parseFloat(data_array[16]);
            obj.add['CDIN11'] = parseFloat(data_array[17]);
            obj.add['CDIN12'] = parseFloat(data_array[18]);
            obj.add['VDIN1'] = parseFloat(data_array[19]);
            obj.add['VDIN2'] = parseFloat(data_array[20]);
            obj.add['VDIN3'] = parseFloat(data_array[21]);
            obj.add['VDIN4'] = parseFloat(data_array[22]);
            obj.add['VDIN5'] = parseFloat(data_array[23]);
            obj.add['VDIN6'] = parseFloat(data_array[24]);
            obj.add['VDIN7'] = parseFloat(data_array[25]);
            obj.add['VDIN8'] = parseFloat(data_array[26]);
            obj.add['VDIN9'] = parseFloat(data_array[27]);
            obj.add['VDIN10'] = parseFloat(data_array[28]);
            obj.add['VDIN11'] = parseFloat(data_array[29]);
            obj.add['VDIN12'] = parseFloat(data_array[30]);
            obj.add['CAIN1'] = parseFloat(data_array[31]);
            obj.add['CAIN2'] = parseFloat(data_array[32]);
            obj.add['CAIN3'] = parseFloat(data_array[33]);
            obj.add['CAIN4'] = parseFloat(data_array[34]);
            obj.add['CAIN5'] = parseFloat(data_array[35]);
            obj.add['CAIN6'] = parseFloat(data_array[36]);
            obj.add['CAIN7'] = parseFloat(data_array[37]);
            obj.add['CAIN8'] = parseFloat(data_array[38]);
            obj.add['CAIN9'] = parseFloat(data_array[39]);
            obj.add['CAIN10'] = parseFloat(data_array[40]);
            obj.add['CAIN11'] = parseFloat(data_array[41]);
            obj.add['CAIN12'] = parseFloat(data_array[42]);
            obj.add['VAIN1'] = parseFloat(data_array[43]);
            obj.add['VAIN2'] = parseFloat(data_array[44]);
            obj.add['VAIN3'] = parseFloat(data_array[45]);
            obj.add['VAIN4'] = parseFloat(data_array[46]);
            obj.add['VAIN5'] = parseFloat(data_array[47]);
            obj.add['VAIN6'] = parseFloat(data_array[48]);
            obj.add['VAIN7'] = parseFloat(data_array[49]);
            obj.add['VAIN8'] = parseFloat(data_array[50]);
            obj.add['VAIN9'] = parseFloat(data_array[51]);
            obj.add['VAIN10'] = parseFloat(data_array[52]);
            obj.add['VAIN11'] = parseFloat(data_array[53]);
            obj.add['VAIN12'] = parseFloat(data_array[54]);
            obj.add['CRC_IND'] = parseFloat(data_array[55]);
            obj.add['CRC_VAL'] = parseFloat(data_array[56]);
        }

        var newPacket = new Packet(obj);
        newPacket.save(function(err) {
            if(err) {
                console.log(err);
            } else {
                console.log("sucessfully saved packet data");
            }
        })

        console.log(obj, "parsed object")
    })
}