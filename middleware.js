import jsonwebtoken from 'jsonwebtoken';

const authenticate = (req, res, next) => {
	const token = req.headers.authorization || '';
	jsonwebtoken.verify(token, secretKey, (error, decoded) => {
		if (error) {
			next({ error: 'token varification failed failed' });
		} else {
			const { expiredAt } = decoded;
			res.locals.id = decoded.id;
			if (expiredAt > new Date().getTime()) {
				next();
			} else {
				next({ error: 'token expired' });
			}
		}
	});
};

const authError = (err, req, res, next) => {
	res.json(err);
};
export { authenticate, authError };
