var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var packetSchema = new mongoose.Schema({
    len: {type:String},
    pkt_name: {type:String},
    // mobile: {type: Number, required: true},
    pkt_type: {type: String},
    device_id: {type: Number},
    date: {type: Number},
    time: {type: Number},
    add: { type: Schema.Types.Mixed},
    created: {type: Date, required: true}
  });

  module.exports = mongoose.model('packet', packetSchema);